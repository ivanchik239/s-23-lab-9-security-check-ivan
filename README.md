# Lab9 -- Security check

Website for testing - https://lichess.org/

1. [Return a consistent message for both existent and non-existent accounts](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#url-tokens:~:text=forgot%20password%20service%3A-,Return%20a%20consistent%20message%20for%20both%20existent%20and%20non%2Dexistent%20accounts,-.)

| Test step                                    | Result                               |
|----------------------------------------------|--------------------------------------|
| Open [lichess.org](https://lichess.org/)     | Ok                                   |
| Click `Войти` and then `Сброс пароля`        | Ok, page with email prompt appears   |
| Input non-existent email and remember result | Ok, page says email been sent        |
| Redo previous steps but input existing email | Ok, again, page says email been sent |

Proof:\
![img.png](img.png)

Test case completed, in both scenarios page is completely the same

2. [Use URL tokens for the simplest and fastest implementation](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#introduction:~:text=Use%20URL%20tokens%20for%20the%20simplest%20and%20fastest%20implementation)

| Test step                            | Result                                               |
|--------------------------------------|------------------------------------------------------|
| Open reset password email            | Ok                                                   |
| Observe URL token in link from email | Ok, query url clearly contains some identifier/token |
| Ensure the link uses HTTPS           | Ok                                                   |
| Follow the link                      | Ok, page for password change opens                   |

Proof:\
![img_1.png](img_1.png)

Test case completed, website uses URL tokens sent by email for password reset

3. [The user should confirm the password they set by writing it twice](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#url-tokens:~:text=The%20user%20should%20confirm%20the%20password%20they%20set%20by%20writing%20it%20twice)

| Test step                                         | Result                                               |
|---------------------------------------------------|------------------------------------------------------|
| Open previous page for password change once again | Ok                                                   |
| Ensure 2 fields for new password                  | Ok                                                   |

Proof:\
![img_2.png](img_2.png)

Test case completed, user writes new password twice

4. [Ensure that a secure password policy is in place, and is consistent with the rest of the application](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#url-tokens:~:text=Ensure%20that%20a%20secure%20password%20policy%20is%20in%20place%2C%20and%20is%20consistent%20with%20the%20rest%20of%20the%20application)

| Test step                                   | Result                                             |
|---------------------------------------------|----------------------------------------------------|
| Input short weak password                   | Ok, like 'qmwn'                                    |
| Ensure such password is rejected by website | Failed, system allows such short and weak password |
| Input very common password                  | Ok, like '123456'                                  |
| Ensure such password is rejected by website | Ok                                                 |

Proof:\
![img_3.png](img_3.png)

Test case is semi-successful, it is risky to allow users create such insecure passwords. At least, they use some dictionary of common passwords

5. [Send the user an email informing them that their password has been reset](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#url-tokens:~:text=Send%20the%20user%20an%20email%20informing%20them%20that%20their%20password%20has%20been%20reset)

| Test step                                   | Result                          |
|---------------------------------------------|---------------------------------|
| After previous steps click `Сменить пароль` | Ok                              |
| Ensure user is logged out                   | Failed, user is still logged in |

Proof:\
![img_4.png](img_4.png)

Test case is failed

6. [Once they have set their new password, the user should then login through the usual mechanism](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#url-tokens:~:text=Once%20they%20have%20set%20their%20new%20password%2C%20the%20user%20should%20then%20login%20through%20the%20usual%20mechanism)

| Test step                                            | Result                   |
|------------------------------------------------------|--------------------------|
| After previous steps check mailbox                   | Ok                       |
| Ensure email with info about password change is sent | Failed, no email is sent |

Test case is failed
